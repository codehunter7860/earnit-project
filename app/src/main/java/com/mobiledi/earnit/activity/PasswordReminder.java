package com.mobiledi.earnit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mobiledi.earnit.R;
import com.mobiledi.earnit.dialogfragment.PasswordReminderResultFragment;
import com.mobiledi.earnit.utils.AppConstant;
import com.mobiledi.earnit.utils.ScreenSwitch;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by GreenLose on 12/7/2017.
 */

public class PasswordReminder extends BaseActivity implements View.OnClickListener, Validator.ValidationListener {

    PasswordReminder passwordReminder;
    ScreenSwitch screenSwitch;
    Button sendButton;
    Validator validator;
    RelativeLayout progressBar;
    ImageButton ivBackArrow;
    EditText child_email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.password_reminder_sign);

        passwordReminder = this;
        screenSwitch = new ScreenSwitch(passwordReminder);
        sendButton = (Button) findViewById(R.id.passwordreminder_send);
        child_email = (EditText) findViewById(R.id.child_email);
        sendButton.setOnClickListener(passwordReminder);
        progressBar = (RelativeLayout) findViewById(R.id.loadingPanel);
        ivBackArrow = (ImageButton) findViewById(R.id.ivBackArrow);
        ivBackArrow.setOnClickListener(passwordReminder);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.passwordreminder_send:

                /*This method is used to check to reset password.*/

                String emailAddress = child_email.getText().toString();

                if (validateEmail(emailAddress)) {
                    try {
                        resetPassWordAndCheckOnYourEmail(emailAddress);
                    } catch (JSONException | UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }

                break;

            case R.id.ivBackArrow:
                startActivity(new Intent(passwordReminder, LoginScreen.class));
                finish();
                break;
        }
    }

    private boolean validateEmail(String emailAddress) {
        if (emailAddress.isEmpty() || !(Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches())) {
            if (emailAddress.isEmpty()) {
                child_email.setError("Enter email address");
            } else {
                child_email.setError("Enter correct email address");
            }

            return false;
        } else {
            child_email.setError(null);
        }

        return true;
    }

    private void resetPassWordAndCheckOnYourEmail(String emailAddress) throws JSONException, UnsupportedEncodingException {
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("EMAIL_ADDRESS", emailAddress);

        /*JSONObject jsonObject = new JSONObject();
        jsonObject.put("EMAIL_ADDRESS", emailAddress);

        StringEntity entity = new StringEntity(jsonObject.toString());*/

        asyncHttpClient.post(AppConstant.BASE_URL + AppConstant.PASSWORD_REMINDER, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] header, JSONObject response) {
                Log.d("Success Code", String.valueOf(statusCode));
                Log.d("Success Response", response.toString());

                try {
                    String passowrdFinal = response.getString("message");
                    String finalPasswordFinal = passowrdFinal.substring(2, passowrdFinal.length() - 2);

                    Toast.makeText(PasswordReminder.this, finalPasswordFinal + "successfully.", Toast.LENGTH_SHORT).show();

                    //validator.validate();
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.password_reminderid_sign, new PasswordReminderResultFragment());
                    ft.commit();

                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.d("Failure Code", String.valueOf(statusCode));
                Log.d("Failure Response", throwable.toString());

                Log.d("BaseURL", AppConstant.BASE_URL + AppConstant.PASSWORD_REMINDER);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                Log.d("Failure Code", String.valueOf(statusCode));
                Log.d("Failure Response", throwable.toString());

                Log.d("BaseURL", AppConstant.BASE_URL + AppConstant.PASSWORD_REMINDER);
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        screenSwitch.moveToLogin();
    }
}
