package com.mobiledi.earnit.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.mobiledi.earnit.R;
import com.mobiledi.earnit.adapter.ItemAdapter;
import com.mobiledi.earnit.model.Child;
import com.mobiledi.earnit.model.Goal;
import com.mobiledi.earnit.model.Item;
import com.mobiledi.earnit.model.Parent;
import com.mobiledi.earnit.model.Tasks;
import com.mobiledi.earnit.utils.AppConstant;
import com.mobiledi.earnit.utils.FloatingMenu;
import com.mobiledi.earnit.utils.NavigationDrawer;
import com.mobiledi.earnit.utils.ScreenSwitch;
import com.mobiledi.earnit.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by mobile-di on 7/10/17.
 */

public class Balance extends BaseActivity implements View.OnClickListener, NavigationDrawer.OnDrawerToggeled {

    TextView headerBalance;
    CircularImageView avatar;
    Balance balance;
    public Parent parentObject;
    public Child childObject, otherChild;
    Goal goal;
    Tasks tasks;
    String fromScreen, userType;
    private final String TAG = "BalanceActivity";
    Intent intent;
    RelativeLayout progressBar;
    ScreenSwitch screenSwitch;
    private Toolbar addToolbar;
    private ImageButton drawerToggle;
    ImageView back;
    Button cancel_btn;
    Button save_btn;
    TextView balanceSetting;
    ArrayList<Item> goalsList;
    private BottomSheetDialog mBottomSheetDialog;
    private String repeat;
    int fetchGoalId = 0;
    String subtract = "Subtract";
    String add = "Add";
    TextView currentBalance,balance_Header;
    EditText task_detailedt,adjustmentedt;
    ImageButton helpIcon,forward_arrow,backward_arrow;

    /* private static String[] title = {
            "Trip to DC",
            "School",
            "Room Clean",
            "Shopping",
            "Golf play"
    };

    private static String[] currentbalancearray = AppConstant.CURRENTBALANCEARRY[0]

    private static String[] total ={
            "2000",
            "1000",
            "500",
            "1500",
            "300"
    };*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.balance_layout);

        balance_Header = (TextView) findViewById(R.id.balance_header);
        forward_arrow = (ImageButton) findViewById(R.id.addtask_forward_arrow);
        backward_arrow = (ImageButton) findViewById(R.id.addtask_backarrow);
        helpIcon = (ImageButton) findViewById(R.id.helpicon);
        adjustmentedt = (EditText) findViewById(R.id.adjustment);
        task_detailedt = (EditText) findViewById(R.id.task_detail);
        currentBalance = (TextView) findViewById(R.id.current_balance);
        balanceSetting = (TextView) findViewById(R.id.add_substract);
        cancel_btn = (Button) findViewById(R.id.cancel);
        save_btn = (Button) findViewById(R.id.save);
        headerBalance = (TextView) findViewById(R.id.goal_header);
        addToolbar = (Toolbar) findViewById(R.id.toolbar_goal);
        drawerToggle = (ImageButton) findViewById(R.id.drawerBtn);
        progressBar = (RelativeLayout) findViewById(R.id.loadingPanel);
        setSupportActionBar(addToolbar);
        getSupportActionBar().setTitle(null);
        goalsList = new ArrayList<>();
        goalsList.add(new Item(0,add));
        goalsList.add(new Item(1,subtract));
        setViewIds();
        screenSwitch = new ScreenSwitch(balance);
        intent = getIntent();
        parentObject = (Parent) intent.getSerializableExtra(AppConstant.PARENT_OBJECT);
        childObject = (Child) intent.getSerializableExtra(AppConstant.CHILD_OBJECT);
        otherChild = (Child) intent.getSerializableExtra(AppConstant.OTHER_CHILD_OBJECT);
        goal = (Goal) intent.getSerializableExtra(AppConstant.GOAL_OBJECT);
        tasks = (Tasks) intent.getSerializableExtra(AppConstant.TO_EDIT);
        fromScreen = intent.getStringExtra(AppConstant.FROM_SCREEN);
        userType = intent.getStringExtra(AppConstant.TYPE);
        headerBalance.setText("Adjust" +" " + childObject.getFirstName() + " " + "Balance");
        balanceSetting.setText("Add");

        if(parentObject!=null){
            NavigationDrawer navigationDrawer = new NavigationDrawer(balance, parentObject,childObject,addToolbar,drawerToggle, AppConstant.PARENT_DASHBOARD, 0);
            navigationDrawer.setOnDrawerToggeled(this);

        }
        else{
            NavigationDrawer navigationDrawer = new NavigationDrawer(balance, parentObject,childObject,addToolbar,drawerToggle, AppConstant.PARENT_DASHBOARD, 0);
            navigationDrawer.setOnDrawerToggeled(this);


        }

        currentBalance.setText(AppConstant.CURRENTBALANCEARRY[0] + " / "+ AppConstant.TITLE[0]);
        balance_Header.setText(AppConstant.TITLE[0]);

        forward_arrow.setOnClickListener(new View.OnClickListener() {
            int i =1 ;
            @Override
            public void onClick(View v) {
                    balance_Header.setText(AppConstant.TITLE[i]);
                    currentBalance.setText(AppConstant.CURRENTBALANCEARRY[i] + " / "+ AppConstant.TITLE[i]);
                    if(i < 4)
                    i++;
            }
        });

        backward_arrow.setOnClickListener(new View.OnClickListener() {
            int j = 4;
            @Override
            public void onClick(View v) {
                balance_Header.setText(AppConstant.TITLE[j]);
                currentBalance.setText(AppConstant.CURRENTBALANCEARRY[j] + " / "+ AppConstant.TITLE[j]);
                if(j > 1)
                    j--;
            }
        });

        balanceSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheetDialog(goalsList, balanceSetting, AppConstant.GOAL);
            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(userType.equalsIgnoreCase(AppConstant.PARENT))
                    onCancelAndBack();
                else
                    screenSwitch.moveToChildDashboard(childObject, progressBar);
            }
        });
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adjustmentedt.getText().toString().length() == 0) {

                    Utils.showToast(balance, getResources().getString(R.string.no_adjustment));
                }
                else if (task_detailedt.getText().toString().length() ==0){
                    Utils.showToast(balance, getResources().getString(R.string.no_balance_reason));
                }
                else {

                    int finalBalance;

                    String adjustBalance = adjustmentedt.getText().toString();
                    int mBalance = Integer.parseInt(adjustBalance);
                    int currentRawBalance = Integer.parseInt(AppConstant.CURRENTBALANCEARRY[0]);

                    if (TextUtils.isEmpty(adjustBalance)){
                        adjustmentedt.setError("Please enter adjustment balance.");
                    }else {
                        adjustmentedt.setError(null);
                    }

                    if (currentRawBalance > 0){

                        if (balanceSetting.getText().toString().equals(add)){
                           /* finalBalance = currentRawBalance + mBalance;
                            currentRawBalance = finalBalance;*/

                            AppConstant.CURRENTBALANCEARRY[0] = String.valueOf(currentRawBalance + mBalance);

                        }else if (balanceSetting.getText().toString().equals(subtract)){
                            /*finalBalance = currentRawBalance - mBalance;
                            currentRawBalance = finalBalance;*/


                            AppConstant.CURRENTBALANCEARRY[0] = String.valueOf(currentRawBalance - mBalance);
                        }

                        Utils.showToast(balance, getResources().getString(R.string.balance_save));
                        if (userType.equalsIgnoreCase(AppConstant.PARENT)) onCancelAndBack();
                        else screenSwitch.moveToChildDashboard(childObject, progressBar);

                    }else {
                        Toast.makeText(Balance.this,"No sufficient .",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        helpIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.showToast(balance, getResources().getString(R.string.balance_help));
            }
        });

        try {
            Picasso.with(balance).load(childObject.getAvatar()).error(R.drawable.default_avatar).into(avatar);
        } catch (Exception e) {
            Picasso.with(balance).load(R.drawable.default_avatar).into(avatar);
            e.printStackTrace();
        }

    }

    private void showBottomSheetDialog(ArrayList<Item> items, final TextView dropDownView, final String type) {
        mBottomSheetDialog = new BottomSheetDialog(this);
        final View view = getLayoutInflater().inflate(R.layout.sheet, null);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new ItemAdapter(items, new ItemAdapter.ItemListener() {
            @Override
            public void onItemClick(Item item) {
                if(type.equalsIgnoreCase(AppConstant.GOAL)){
                    fetchGoalId = item.getId();
                }else{
                    repeat = item.getmTitle();
                }
                switch (item.getId()){
                    case 0:{
                        balanceSetting.setText(add);
                        mBottomSheetDialog.dismiss();
                    }
                        break;
                    case 1:{
                        balanceSetting.setText(subtract);
                        mBottomSheetDialog.dismiss();
                    }
                    break;
                }
            }
        }));

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    public void setViewIds() {
        balance = this;
        back = (ImageView) findViewById(R.id.addtask_back_arrow);
        avatar = (CircularImageView) findViewById(R.id.goal_avatar);
        progressBar = (RelativeLayout) findViewById(R.id.loadingPanel);

        back.setOnClickListener(balance);
        avatar.setOnClickListener(balance);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addtask_back_arrow:
                if(userType.equalsIgnoreCase(AppConstant.PARENT))
                    onCancelAndBack();
                else
                    screenSwitch.moveToChildDashboard(childObject, progressBar);
                break;

            case R.id.goal_avatar:
                if(userType.equalsIgnoreCase(AppConstant.PARENT))
                    new FloatingMenu(balance).fetchAvatarDimension(avatar,childObject, otherChild, parentObject, AppConstant.BALANCE_SCREEN, progressBar,null );
                else
                    new FloatingMenu(balance).fetchAvatarDimension(avatar, childObject, parentObject, AppConstant.BALANCE_SCREEN, progressBar);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(userType.equalsIgnoreCase(AppConstant.PARENT))
             onCancelAndBack();
        else
            screenSwitch.moveToChildDashboard(childObject, progressBar);
    }
    public void onCancelAndBack() {
        if(fromScreen.equalsIgnoreCase(AppConstant.PARENT_DASHBOARD)) {
            screenSwitch.moveToParentDashboard(parentObject);
        }else if(fromScreen.equalsIgnoreCase(AppConstant.CHECKED_IN_SCREEN)){
            if(otherChild.getTasksArrayList().size() > 0)
                screenSwitch.moveToAllTaskScreen(childObject, otherChild,fromScreen, parentObject,AppConstant.BALANCE_SCREEN );
            else
                Utils.showToast(balance, getResources().getString(R.string.no_task_available));
        }else if(fromScreen.equalsIgnoreCase(AppConstant.CHECKED_IN_TASK_APPROVAL__SCREEN)){
            if (childObject.getTasksArrayList().size() > 0)
                screenSwitch.moveToAllTaskScreen(childObject, otherChild,fromScreen, parentObject,AppConstant.BALANCE_SCREEN );
            else
                Utils.showToast(balance, getResources().getString(R.string.no_task_for_approval));
        }else if(fromScreen.equalsIgnoreCase(AppConstant.ADD_TASK))
            screenSwitch.moveToAddTask(childObject, otherChild, parentObject, fromScreen, tasks);
        else if(fromScreen.equalsIgnoreCase(AppConstant.GOAL_SCREEN))
            screenSwitch.isGoalExists(childObject, otherChild, parentObject, progressBar,fromScreen,tasks);
        else
            screenSwitch.moveToAllTaskScreen(childObject, otherChild,AppConstant.CHECKED_IN_SCREEN, parentObject,AppConstant.BALANCE_SCREEN );
    }

    @Override
    public void onDrawerToggeled() {

    }
}
