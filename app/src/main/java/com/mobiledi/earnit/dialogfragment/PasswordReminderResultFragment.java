package com.mobiledi.earnit.dialogfragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.mobiledi.earnit.R;
import com.mobiledi.earnit.activity.PasswordReminder;
import com.mobiledi.earnit.utils.ScreenSwitch;

/**
 * Created by GreenLose on 12/8/2017.
 */

public class PasswordReminderResultFragment extends Fragment {

    ScreenSwitch screenSwitch;
    PasswordReminderResultFragment resultFragment;
    PasswordReminder passwordReminder_;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View PasswordReminderView = inflater.inflate(R.layout.password_reminder_result, container, false);
        resultFragment = this;
        screenSwitch = new ScreenSwitch(passwordReminder_);
        Button okBtn = (Button) PasswordReminderView.findViewById(R.id.ok_button);

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getView().getContext(), "Pressed OK key!!!", Toast.LENGTH_LONG ).show();
                onBackPressed();

            }
        });

        return PasswordReminderView;

    }

    private void onBackPressed() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.popBackStack();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
