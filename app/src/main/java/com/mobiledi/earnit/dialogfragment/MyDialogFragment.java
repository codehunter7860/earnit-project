package com.mobiledi.earnit.dialogfragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mobiledi.earnit.R;

import fr.tvbarthel.lib.blurdialogfragment.BlurDialogFragment;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;


public class MyDialogFragment extends BlurDialogFragment {

    private final String LOG_TAG = MyDialogFragment.class.getSimpleName();
            Boolean checkButton = false;
    // onCreate --> (onCreateDialog) --> onCreateView --> onActivityCreated

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.v(LOG_TAG, "onCreateView");

        View dialogView = inflater.inflate(R.layout.repeat_daily, container, false);

        TextView repeat_daily_text = (TextView) dialogView.findViewById(R.id.repeat_daily_frequency);
        repeat_daily_text.setText("Daily");

        final Button repeat_daily_check = (Button) dialogView.findViewById(R.id.repeat_daily_checkbox);
        repeat_daily_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkButton) {
                    repeat_daily_check.setText("");
                    checkButton = false;
                }
                else {
                    repeat_daily_check.setText("✔");
                    checkButton = true;
                }
            }
        });

        // "Got it" button
        Button buttonPos = (Button) dialogView.findViewById(R.id.repeatdaily_ok);
        buttonPos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Dismiss the DialogFragment (remove it from view)
                dismiss();
            }
        });

        // "Cancel" button
        Button buttonNeg = (Button) dialogView.findViewById(R.id.repeatdaily_cancel);
        buttonNeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // If shown as dialog, cancel the dialog (cancel --> dismiss)
                if (getShowsDialog())
                    getDialog().cancel();
                // If shown as Fragment, dismiss the DialogFragment (remove it from view)
                else
                    dismiss();
            }
        });

        return dialogView;
    }

    // If shown as dialog, set the width of the dialog window
    // onCreateView --> onActivityCreated -->  onViewStateRestored --> onStart --> onResume
    @Override
    public void onResume() {
        super.onResume();
        Log.v(LOG_TAG, "onResume");
        if (getShowsDialog()) {
            // Set the width of the dialog to the width of the screen in portrait mode
            DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
            int dialogWidth = Math.min(metrics.widthPixels, metrics.heightPixels);
            getDialog().getWindow().setLayout(dialogWidth, WRAP_CONTENT);
        }
    }

    private void showToast(String buttonName) {
    }

    // If dialog is cancelled: onCancel --> onDismiss
    @Override
    public void onCancel(DialogInterface dialog) {
        Log.v(LOG_TAG, "onCancel");
    }

    // If dialog is cancelled: onCancel --> onDismiss
    // If dialog is dismissed: onDismiss
    @Override
    public void onDismiss(DialogInterface dialog) {
        Log.v(LOG_TAG,"onDismiss");
    }

    public void show(FragmentManager fm, String string) {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected float getDownScaleFactor() {

        return 5.0f;
    }

    @Override
    protected int getBlurRadius() {
        // Allow to customize the blur radius factor.
        return 5;
    }

    @Override
    protected boolean isActionBarBlurred() {
        return true;
    }

    @Override
    protected boolean isDimmingEnable() {
        return true;
    }

    @Override
    protected boolean isRenderScriptEnable() {
        return true;
    }

    @Override
    protected boolean isDebugEnable() {
        return false;
    }
}
