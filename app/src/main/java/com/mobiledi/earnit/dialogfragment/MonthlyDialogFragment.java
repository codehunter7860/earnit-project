package com.mobiledi.earnit.dialogfragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.mobiledi.earnit.R;

import java.util.ArrayList;
import java.util.List;

import fr.tvbarthel.lib.blurdialogfragment.BlurDialogFragment;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static com.mobiledi.earnit.dialogfragment.WeeklyDialogFragment.weekList;


public class MonthlyDialogFragment extends BlurDialogFragment {


    public static List<String> monthList = new ArrayList<>();

    Button daybutton1;Button daybutton2;Button daybutton3;Button daybutton4;
    Button daybutton5;Button daybutton6;Button daybutton7;Button daybutton8;
    Button daybutton9;Button daybutton10;Button daybutton11;Button daybutton12;
    Button daybutton13;Button daybutton14;Button daybutton15;Button daybutton16;
    Button daybutton17;Button daybutton18;Button daybutton19;Button daybutton20;
    Button daybutton21;Button daybutton22;Button daybutton23;Button daybutton24;
    Button daybutton25;Button daybutton26;Button daybutton27;Button daybutton28;
    Button daybutton29;Button daybutton30;Button daybutton31;

    Boolean D1 = false;
    Boolean D2 = false;
    Boolean D3 = false;
    Boolean D4 = false;
    Boolean D5 = false;
    Boolean D6 = false;
    Boolean D7 = false;
    Boolean D8 = false;
    Boolean D9 = false;
    Boolean D10 = false;
    Boolean D11 = false;
    Boolean D12 = false;
    Boolean D13 = false;
    Boolean D14 = false;
    Boolean D15 = false;
    Boolean D16 = false;
    Boolean D17 = false;
    Boolean D18 = false;
    Boolean D19 = false;
    Boolean D20 = false;
    Boolean D21 = false;
    Boolean D22 = false;
    Boolean D23 = false;
    Boolean D24 = false;
    Boolean D25 = false;
    Boolean D26 = false;
    Boolean D27 = false;
    Boolean D28 = false;
    Boolean D29 = false;
    Boolean D30 = false;
    Boolean D31 = false;

    Boolean selectSwitch = true;

    private final String LOG_TAG = MyDialogFragment.class.getSimpleName();
    Boolean checkButton = false;
    Boolean radiocheckButton = false;
    // onCreate --> (onCreateDialog) --> onCreateView --> onActivityCreated
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.v(LOG_TAG, "onCreateView");

        View dialogView = inflater.inflate(R.layout.repeat_monthly, container, false);

        TextView repeat_daily_text = (TextView) dialogView.findViewById(R.id.repeat_monthly_frequency);
        repeat_daily_text.setText("Monthly");







        final Button repeat_monthly_check = (Button) dialogView.findViewById(R.id.repeat_monthly_checkbox);
        repeat_monthly_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkButton) {
                    repeat_monthly_check.setText("");
                    checkButton = false;
                }
                else {
                    repeat_monthly_check.setText("✔");
                    checkButton = true;
                }
            }
        });


        daybutton1 = (Button) dialogView.findViewById(R.id.d1);
        daybutton1.setText("1");
        daybutton2 = (Button) dialogView.findViewById(R.id.d2);
        daybutton2.setText("2");
        daybutton3 = (Button) dialogView.findViewById(R.id.d3);
        daybutton3.setText("3");
        daybutton4 = (Button) dialogView.findViewById(R.id.d4);
        daybutton4.setText("4");
        daybutton5 = (Button) dialogView.findViewById(R.id.d5);
        daybutton5.setText("5");
        daybutton6 = (Button) dialogView.findViewById(R.id.d6);
        daybutton6.setText("6");
        daybutton7 = (Button) dialogView.findViewById(R.id.d7);
        daybutton7.setText("7");
        daybutton8 = (Button) dialogView.findViewById(R.id.d8);
        daybutton8.setText("8");
        daybutton9 = (Button) dialogView.findViewById(R.id.d9);
        daybutton9.setText("9");
        daybutton10 = (Button) dialogView.findViewById(R.id.d10);
        daybutton10.setText("10");
        daybutton11 = (Button) dialogView.findViewById(R.id.d11);
        daybutton11.setText("11");
        daybutton12 = (Button) dialogView.findViewById(R.id.d12);
        daybutton12.setText("12");
        daybutton13 = (Button) dialogView.findViewById(R.id.d13);
        daybutton13.setText("13");
        daybutton14 = (Button) dialogView.findViewById(R.id.d14);
        daybutton14.setText("14");
        daybutton15 = (Button) dialogView.findViewById(R.id.d15);
        daybutton15.setText("15");
        daybutton16 = (Button) dialogView.findViewById(R.id.d16);
        daybutton16.setText("16");
        daybutton17 = (Button) dialogView.findViewById(R.id.d17);
        daybutton17.setText("17");
        daybutton18 = (Button) dialogView.findViewById(R.id.d18);
        daybutton18.setText("18");
        daybutton19 = (Button) dialogView.findViewById(R.id.d19);
        daybutton19.setText("19");
        daybutton20 = (Button) dialogView.findViewById(R.id.d20);
        daybutton20.setText("20");
        daybutton21 = (Button) dialogView.findViewById(R.id.d21);
        daybutton21.setText("21");
        final Button  daybutton22 = (Button) dialogView.findViewById(R.id.d22);
        daybutton22.setText("22");
        daybutton23 = (Button) dialogView.findViewById(R.id.d23);
        daybutton23.setText("23");
        daybutton24 = (Button) dialogView.findViewById(R.id.d24);
        daybutton24.setText("24");
        daybutton25 = (Button) dialogView.findViewById(R.id.d25);
        daybutton25.setText("25");
        daybutton26 = (Button) dialogView.findViewById(R.id.d26);
        daybutton26.setText("26");
        daybutton27 = (Button) dialogView.findViewById(R.id.d27);
        daybutton27.setText("27");
        daybutton28 = (Button) dialogView.findViewById(R.id.d28);
        daybutton28.setText("28");
        daybutton29 = (Button) dialogView.findViewById(R.id.d29);
        daybutton29.setText("29");
        daybutton30 = (Button) dialogView.findViewById(R.id.d30);
        daybutton30.setText("30");
        daybutton31 = (Button) dialogView.findViewById(R.id.d31);
        daybutton31.setText("31");


        final RadioButton eachButton = (RadioButton) dialogView.findViewById(R.id.repeatmonthly_eachradio);
        final RadioButton onButton = (RadioButton) dialogView.findViewById(R.id.repeatmonthly_ontheradio);

        eachButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (eachButton.isChecked()){
                    onButton.setChecked(false);
                    selectSwitch = true;

                }
            }
        });
        onButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onButton.isChecked()){
                    selectSwitch = false;
                    eachButton.setChecked(false);
                    daybutton1.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton2.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton3.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton4.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton5.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton6.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton7.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton8.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton9.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton10.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton11.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton12.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton13.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton14.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton15.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton16.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton17.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton18.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton19.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton20.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton21.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton22.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton23.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton24.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton25.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton26.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton27.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton28.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton29.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton30.setBackgroundResource(R.drawable.repeat_calendar);
                    daybutton31.setBackgroundResource(R.drawable.repeat_calendar);
                }
            }
        });



        daybutton1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D1) {
                    daybutton1.setBackgroundResource(R.drawable.repeat_calendar);
                    D1 = false;
                } else {
                    monthList.add("1");
                    daybutton1.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D1 = true;
                }
            }
        }
    });

    daybutton2.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D2) {
                    daybutton2.setBackgroundResource(R.drawable.repeat_calendar);
                    D2 = false;
                } else {
                    monthList.add("2");

                    daybutton2.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D2 = true;
                }
            }
        }
    });

    daybutton3.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D3) {
                    daybutton3.setBackgroundResource(R.drawable.repeat_calendar);
                    D3 = false;
                } else {
                    monthList.add("3");

                    daybutton3.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D3 = true;
                }
            }
        }
    });

    daybutton4.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D4) {
                    daybutton4.setBackgroundResource(R.drawable.repeat_calendar);
                    D4 = false;
                } else {
                    monthList.add("4");
                    daybutton4.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D4 = true;
                }
            }
        }
    });

    daybutton5.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D5) {
                    daybutton5.setBackgroundResource(R.drawable.repeat_calendar);
                    D5 = false;
                } else {
                    monthList.add("5");
                    daybutton5.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D5 = true;
                }
            }
        }
    });

    daybutton6.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D6) {
                    daybutton6.setBackgroundResource(R.drawable.repeat_calendar);
                    D6 = false;
                } else {
                    monthList.add("6");
                    daybutton6.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D6 = true;
                }
            }
        }
    });

    daybutton7.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D7) {
                    daybutton7.setBackgroundResource(R.drawable.repeat_calendar);
                    D7 = false;
                } else {
                    monthList.add("7");
                    daybutton7.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D7 = true;
                }
            }
        }
    });
    daybutton8.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D8) {
                    daybutton8.setBackgroundResource(R.drawable.repeat_calendar);
                    D8 = false;
                } else {
                    monthList.add("8");
                    daybutton8.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D8 = true;
                }
            }
        }
    });
    daybutton9.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D9) {
                    daybutton9.setBackgroundResource(R.drawable.repeat_calendar);
                    D9 = false;
                } else {
                    monthList.add("9");
                    daybutton9.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D9 = true;
                }
            }
        }
    });
    daybutton10.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D10) {
                    daybutton10.setBackgroundResource(R.drawable.repeat_calendar);
                    D10 = false;
                } else {
                    monthList.add("10");
                    daybutton10.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D10 = true;
                }
            }
        }
    });
    daybutton11.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D11) {
                    daybutton11.setBackgroundResource(R.drawable.repeat_calendar);
                    D11 = false;
                } else {
                    monthList.add("11");
                    daybutton11.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D11 = true;
                }
            }
        }
    });
    daybutton12.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D12) {

                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("1")) {

                            weekList.remove(i);
                        }
                    }

                    daybutton12.setBackgroundResource(R.drawable.repeat_calendar);
                    D12 = false;
                } else {
                    monthList.add("12");
                    daybutton12.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D12 = true;
                }
            }
        }
    });
    daybutton13.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D13) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("1")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton13.setBackgroundResource(R.drawable.repeat_calendar);
                    D13 = false;
                } else {
                    monthList.add("13");

                    daybutton13.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D13 = true;
                }
            }
        }
    });
    daybutton14.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D14) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("1")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton14.setBackgroundResource(R.drawable.repeat_calendar);
                    D14 = false;
                } else {
                    daybutton14.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D14 = true;
                }
            }
        }
    });
    daybutton15.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D15) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("1")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton15.setBackgroundResource(R.drawable.repeat_calendar);
                    D7 = false;
                } else {
                    daybutton15.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D15 = true;
                }
            }
        }
    });
    daybutton16.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D16) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("1")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton16.setBackgroundResource(R.drawable.repeat_calendar);
                    D16 = false;
                } else {
                    daybutton16.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D16 = true;
                }
            }
        }
    });
    daybutton17.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D17) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("1")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton17.setBackgroundResource(R.drawable.repeat_calendar);
                    D17 = false;
                } else {
                    daybutton17.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D17 = true;
                }
            }
        }
    });
    daybutton18.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D18) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("1")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton18.setBackgroundResource(R.drawable.repeat_calendar);
                    D18 = false;
                } else {
                    daybutton18.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D18 = true;
                }
            }
        }
    });
    daybutton19.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D19) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("1")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton19.setBackgroundResource(R.drawable.repeat_calendar);
                    D19 = false;
                } else {
                    daybutton19.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D19 = true;
                }
            }
        }
    });
    daybutton20.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D20) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("2")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton20.setBackgroundResource(R.drawable.repeat_calendar);
                    D20 = false;
                } else {
                    daybutton20.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D20 = true;
                }
            }
        }
    });
    daybutton21.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D21) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("2")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton21.setBackgroundResource(R.drawable.repeat_calendar);
                    D21 = false;
                } else {
                    daybutton21.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D21 = true;
                }
            }
        }
    });
    daybutton22.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D22) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("2")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton22.setBackgroundResource(R.drawable.repeat_calendar);
                    D22 = false;
                } else {
                    daybutton22.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D22 = true;
                }
            }
        }
    });
    daybutton23.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D23) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("2")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton23.setBackgroundResource(R.drawable.repeat_calendar);
                    D23 = false;
                } else {
                    daybutton23.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D23 = true;
                }
            }
        }
    });
    daybutton24.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D24) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("2")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton24.setBackgroundResource(R.drawable.repeat_calendar);
                    D24 = false;
                } else {
                    daybutton24.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D24 = true;
                }
            }
        }
    });
    daybutton25.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D25) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("2")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton25.setBackgroundResource(R.drawable.repeat_calendar);
                    D25 = false;
                } else {
                    daybutton25.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D25 = true;
                }
            }
        }
    });
    daybutton26.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D26) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("2")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton26.setBackgroundResource(R.drawable.repeat_calendar);
                    D26 = false;
                } else {
                    daybutton26.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D26 = true;
                }
            }
        }
    });
    daybutton27.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D27) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("2")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton27.setBackgroundResource(R.drawable.repeat_calendar);
                    D27 = false;
                } else {
                    daybutton27.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D27 = true;
                }
            }
        }
    });
    daybutton28.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D28) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("2")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton28.setBackgroundResource(R.drawable.repeat_calendar);
                    D28 = false;
                } else {
                    daybutton28.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D28 = true;
                }
            }
        }
    });
    daybutton29.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D29) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("2")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton29.setBackgroundResource(R.drawable.repeat_calendar);
                    D29 = false;
                } else {
                    daybutton29.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D29 = true;
                }
            }
        }
    });
    daybutton30.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D30) {
                    for(int i=0;i<monthList.size();i++){

                        if (monthList.get(i).equalsIgnoreCase("3")) {

                            weekList.remove(i);
                        }
                    }
                    daybutton30.setBackgroundResource(R.drawable.repeat_calendar);
                    D30 = false;
                } else {
                    daybutton30.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D30 = true;
                }
            }
        }
    });
    daybutton31.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(selectSwitch) {
                if (D31) {
                    daybutton31.setBackgroundResource(R.drawable.repeat_calendar);
                    D31 = false;
                } else {
                    daybutton31.setBackgroundResource(R.drawable.repeat_calendar_pink);
                    D31 = true;
                }
            }
        }
    });


        // "Got it" button
        Button buttonPos = (Button) dialogView.findViewById(R.id.repeatmonthly_ok);
        buttonPos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("Pressed OK Button!");
                // Dismiss the DialogFragment (remove it from view)
                dismiss();
            }
        });

        // "Cancel" button
        Button buttonNeg = (Button) dialogView.findViewById(R.id.repeatmonthly_cancel);
        buttonNeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("Pressed Cancel Button!");
                // If shown as dialog, cancel the dialog (cancel --> dismiss)
                if (getShowsDialog())
                    getDialog().cancel();
                    // If shown as Fragment, dismiss the DialogFragment (remove it from view)
                else
                    dismiss();
            }
        });

        return dialogView;
    }

    // If shown as dialog, set the width of the dialog window
    // onCreateView --> onActivityCreated -->  onViewStateRestored --> onStart --> onResume
    @Override
    public void onResume() {
        super.onResume();
        Log.v(LOG_TAG, "onResume");
        if (getShowsDialog()) {
            // Set the width of the dialog to the width of the screen in portrait mode
            DisplayMetrics metrics = getActivity().getResources().getDisplayMetrics();
            int dialogWidth = Math.min(metrics.widthPixels, metrics.heightPixels);
            getDialog().getWindow().setLayout(dialogWidth, WRAP_CONTENT);
        }
    }

    private void showToast(String buttonName) {
    //    Toast.makeText(getActivity(), "Clicked on \"" + buttonName + "\"", Toast.LENGTH_SHORT).show();
    }

    // If dialog is cancelled: onCancel --> onDismiss
    @Override
    public void onCancel(DialogInterface dialog) {
        Log.v(LOG_TAG, "onCancel");
    }

    // If dialog is cancelled: onCancel --> onDismiss
    // If dialog is dismissed: onDismiss
    @Override
    public void onDismiss(DialogInterface dialog) {
        Log.v(LOG_TAG,"onDismiss");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected float getDownScaleFactor() {

        return 5.0f;
    }

    @Override
    protected int getBlurRadius() {
        // Allow to customize the blur radius factor.
        return 5;
    }

    @Override
    protected boolean isActionBarBlurred() {
        return true;
    }

    @Override
    protected boolean isDimmingEnable() {
        return true;
    }

    @Override
    protected boolean isRenderScriptEnable() {
        return true;
    }

    @Override
    protected boolean isDebugEnable() {
        return false;
    }
}
